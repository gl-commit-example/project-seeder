# Project Seeder

This node script will export an existing project on GitLab and import it into a new namespace.

Optionally:
- you can also add a user into the new project (`--add-username "username"`)
- you can also add an expiration to the new user. It should follow the `YYYY-MM-DD` format (`--add-user-expiration "YYYY-MM-DD"`)
- auto trigger a pipeline for a specific branch (`--run-branch-pipeline "branch-name"`)

## Program Parameters

To view the program parameters, run `node index.js --help`

## Running this program (Locally)

1. `yarn install`
1. `node index.js --token "token" --project-id "project-id" --new-project-name "project-name" --new-namespace-name "namespace"`

## Running this program (GitLab CI)

1. Go to the pipelines page for the project
1. Click the `Run Pipeline` button
1. Input the variable key and variable value that you would pass into the program (as though you would if running locally). For example, add the variable key `TOKEN` with the variable value of your GitLab private token
