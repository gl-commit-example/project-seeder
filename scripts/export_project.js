const fs = require('fs');
const axios = require('axios');

const utils = require('./utils');
const { logInfo } = require('./log');

function requestExportDownload({ apiUrl, token}, projectId) {
  return axios.post(`${apiUrl}/projects/${projectId}/export`, {}, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

async function checkExportStatus({ apiUrl, token}, projectId) {
  const { data } = await axios.get(`${apiUrl}/projects/${projectId}/export`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });

  const status = data.export_status;

  if (status === 'failed') {
    throw new Error('Export failed with this response: ' + JSON.stringify(data, null, 2));
  }

  return status;
}

async function waitForExportFinish(apiConfig, projectId) {
  logInfo('Waiting for export to finish:');

  await utils.waitForImportExportToFinish(checkExportStatus, apiConfig, projectId);
}

function downloadExport({ apiUrl, token }, projectId, filePath) {
  return axios.get(`${apiUrl}/projects/${projectId}/export/download`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
    responseType: 'stream'
  }).then((response) => {
    return new Promise((resolve, reject) => {
      const w = fs.createWriteStream(filePath);

      response.data.pipe(w);

      w.on('finish', () => resolve());
      w.on('error', (e) => reject(e));
    });
  });
}

module.exports = {
  downloadExport,
  requestExportDownload,
  waitForExportFinish,
}
