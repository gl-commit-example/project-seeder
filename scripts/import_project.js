const exec = require('child_process').exec;
const axios = require('axios');

const utils = require('./utils');
const { logError, logInfo, log } = require('./log');

function importProject({ apiUrl, token }, namespace, path, filePath) {
  // Was not able to get axios to work properly, had to invoke CURL (was an example in the documentation)
  const args = `--request POST --header "PRIVATE-TOKEN: ${token}" --form "path=${path}" --form "namespace=${namespace}" --form "file=@/${filePath}" ${apiUrl}/projects/import`;

  const command = `curl ${args}`;
  return new Promise((resolve, reject) => {
    exec(command, function (error, stdout, stderr) {
      if (error) {
        logError(error);
        reject(stderr);
      } else {
        log(JSON.parse(stdout))
        resolve(JSON.parse(stdout));
      }
    });
   });
}

async function checkImportStatus({ apiUrl, token}, projectId) {
  const { data } = await axios.get(`${apiUrl}/projects/${projectId}/import`, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });

  const status = data.import_status;

  if (status === 'failed') {
    throw new Error('Import failed with this response: ' + JSON.stringify(data, null, 2));
  }

  return status
}

async function waitForImportFinish(apiConfig, projectId) {
  logInfo('Waiting for import to finish:');

  await utils.waitForImportExportToFinish(checkImportStatus, apiConfig, projectId);

  return projectId;
}

module.exports = {
  importProject,
  waitForImportFinish,
}
