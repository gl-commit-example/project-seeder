const axios = require('axios');

function runPipeline({ apiUrl, token}, projectId, branchName) {
  return axios.post(`${apiUrl}/projects/${projectId}/pipeline?ref=${branchName}`, {}, {
    headers: {
      'PRIVATE-TOKEN': token,
    },
  });
}

module.exports = {
  runPipeline,
};
