const { MAX_EXPORT_TIME, EXPORT_WAIT_TIMEOUT } = require('./constants');
const { logInfo } = require('./log');

async function waitForImportExportToFinish(checkFn, ...args) {
  const maxLoops = MAX_EXPORT_TIME / EXPORT_WAIT_TIMEOUT;
  let currentLoop = 0;
  let status = '';

  do {
    if (status !== 'finished') {
      await new Promise(resolve => setTimeout(resolve, EXPORT_WAIT_TIMEOUT));
    }

    status = await checkFn(...args);
    logInfo('\tcurrent status:', status);

    currentLoop++;

  } while (status !== 'finished' && currentLoop < maxLoops);

  if (currentLoop >= maxLoops) {
    throw new Error(`Maximum wait time (${MAX_EXPORT_TIME / 1000} seconds) exceeded`);
  }
}



module.exports = {
  waitForImportExportToFinish,
}
