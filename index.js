const path = require('path');
const program = require('commander');

const { MAX_EXPORT_TIME, EXPORT_WAIT_TIMEOUT } = require('./scripts/constants');
const { logError, logInfo, log } = require('./scripts/log');
const importApi = require('./scripts/import_project');
const exportApi = require('./scripts/export_project');
const userApi = require('./scripts/user');
const pipelineApi = require('./scripts/pipeline');

const API_URL = 'https://gitlab.com/api/v4';
const EXPORT_FILENAME = 'export.tar.gz';
const FILE_PATH = path.resolve(__dirname, EXPORT_FILENAME);

program
  .version('1.0.0')
  .option('-t, --token <token>', 'GitLab private token (Required)')
  .option('-pi, --project-id <project-id>', 'Project ID of original project (Required)')
  .option('-au, --add-username <username>', 'Username to add to new project (Optional)')
  .option('-aue, --add-user-expiration <YYYY-MM-DD>', 'Expiration date of new user that was added to project (Optional)')
  .option('-rbp, --run-branch-pipeline <branch-name>', 'Run pipeline for branch (Optional)')
  .option('-npn, --new-project-name <new-project-name>', 'Name of new project (Required)')
  .option('-nnn, --new-namespace-name <new-namespace-name>', 'Namespace of new project (Required)')
  .parse(process.argv);

const TOKEN = program.token;
const PROJECT_ID = program.projectId;
const USERNAME_TO_ADD = program.addUsername;
const NEW_NAMESPACE = program.newNamespaceName;
const NEW_PROJECT_NAME = program.newProjectName;
const BRANCH_PIPELINE = program.runBranchPipeline;
const ADD_USER_EXPIRATION = program.addUserExpiration;

logInfo(`Importing project (${PROJECT_ID}) to ${NEW_NAMESPACE}/${NEW_PROJECT_NAME}`);

const apiConfig = { apiUrl: API_URL, token: TOKEN };

exportApi.requestExportDownload(apiConfig, PROJECT_ID)
  .then(exportApi.waitForExportFinish.bind(this, apiConfig, PROJECT_ID))
  .then(exportApi.downloadExport.bind(this, apiConfig, PROJECT_ID, FILE_PATH))
  // Wait between download and import to reduce intermittent API failures
  .then(() => new Promise(resolve => setTimeout(resolve, EXPORT_WAIT_TIMEOUT)))
  .then(importApi.importProject.bind(this, apiConfig, NEW_NAMESPACE, NEW_PROJECT_NAME, FILE_PATH))
  .then(({ id }) => importApi.waitForImportFinish(apiConfig, id))
  .then(async (projectId) => {
    if (USERNAME_TO_ADD) {
      const consoleMessage = `Adding user @${USERNAME_TO_ADD} to the new project`;

      if (ADD_USER_EXPIRATION) {
        logInfo(`${consoleMessage} with an expiration of ${ADD_USER_EXPIRATION}`);
      } else {
        logInfo(`${consoleMessage} with no expiration`);
      }

      const userId = await userApi.getUserId(apiConfig, USERNAME_TO_ADD);
      const result = await userApi.addUserToProject(apiConfig, userId, projectId, ADD_USER_EXPIRATION);
      log(result.data);
    }

    if (BRANCH_PIPELINE) {
      logInfo(`Triggering pipeline for ${BRANCH_PIPELINE} in the new project`);
      const result = await pipelineApi.runPipeline(apiConfig, projectId, BRANCH_PIPELINE);
      log(result.data);
    }
  })
  .then((result) => log(result))
  .catch(error => logError(error));
